

dataPath = "./multiclass_data_final/"
countsRawPath = "countsRawFinal_multi.csv"
countsNormalizedPath = "countsNormalizedFinal_multi.csv"
samplePath = "samplesFinal_multi.csv"




sampleInfo = read.csv2(paste0(dataPath, samplePath))#, sep = "\t")
countsRaw = read.csv2(paste0(dataPath, countsRawPath))#, sep = "\t")
rownames(countsRaw) = countsRaw[,1]
countsRaw = countsRaw[, -1]
countsAll = read.csv2(paste0(dataPath, countsNormalizedPath))#, sep = "\t")
rownames(countsAll) = countsAll[,1]
countsAll = countsAll[, -1]
 
load(paste0(dataPath, "dgeGenesEnsembl75.RData"))

source("preprocessingThrombo.R") 


########################################
####################Data annotation

# loading the annotation file
gtfPath19 = "/grch19/gencode.v19.chr_patch_hapl_scaff.annotation.gtf"
ann = read.delim(gtfPath19, skip = 5, header = FALSE)

ann_gene = ann[which(ann$V3 == "gene"),] 
ann_gene = ann_gene[grep("gene_status KNOWN", ann_gene$V9),]
splits = strsplit(ann_gene$V9, split = ";")
splitsFull = strsplit(ann$V9, split = ";") 
fsplits2 = rownames(countsRaw)
fsplits = substr(unlist(lapply(splits, `[[`, 1)), 9, nchar(unlist(lapply(splits, `[[`, 1)))) 
fsplits = gsub(x = fsplits, "\\..*","") 
fsplitsMatches = match(fsplits2, fsplits)
fsplitsNonMatched = which(is.na(fsplitsMatches))
if(length(fsplitsNonMatched) > 0)
{
  fsplitsMatches = fsplitsMatches[-fsplitsNonMatched]
  countsRaw = countsRaw[-fsplitsNonMatched,]  
}
ann_gene = ann_gene[fsplitsMatches,]
splits = strsplit(ann_gene$V9, split = ";")
fsplits = substr(unlist(lapply(splits, `[[`, 1)), 9, nchar(unlist(lapply(splits, `[[`, 1)))) 
fsplits = gsub(x = fsplits, "\\..*","") 
gnsplits = substr(unlist(lapply(splits, `[[`, 5)), 12, nchar(unlist(lapply(splits, `[[`, 5))))  
duplicatedId = which(duplicated(gnsplits)==T)
if(length(duplicatedId) > 0)
{
  gnsplits = gnsplits[-duplicatedId]
  fsplits = fsplits[-duplicatedId]
  ann_gene = ann_gene[-duplicatedId, ]
  countsRaw = countsRaw[-duplicatedId,]
}  
countsAll = countsAll[rownames(countsRaw), ]
genes = genes[rownames(countsRaw),]

####################################################
####################Setting up the image templates
library(edgeR) 
source("./statisticalAnalysis.R")
library(MatrixGenerics)
matrixTemplate = read.table("./Map_old_kegg_by_ensg_243.tsv", sep = "\t")
for(i in 1:nrow(matrixTemplate))
{
  for(j in 1:ncol(matrixTemplate))
  {
    if(matrixTemplate[i,j] != "0")
    {
      id = grep(matrixTemplate[i,j], row.names(countsAll))
      if(length(id) > 0)
        matrixTemplate[i,j] = id
    }
  }
} 
for(i in 1:ncol(matrixTemplate))
  matrixTemplate[,i] = as.numeric(matrixTemplate[, i]) 
 
pictureWidth = ncol(matrixTemplate)
pictureHeight = nrow(matrixTemplate)


prepareMatrixFromMap = function(dataFiltered, matrixTemplate, pictureWidth, pictureHeight,col)
{
  output = matrix(0, nrow = pictureHeight, ncol = pictureWidth)
  for(i in 1:nrow(output))
  {
    nonZero = which(matrixTemplate[i,] != 0)
    output[i, 1:length(nonZero)] = dataFiltered[unlist(matrixTemplate[i, nonZero]), col]
  }
  return(output)
}



##########################################
####################Input image generation


#helper function for foled/directory creation
createDirIfNotExists = function(mainDir, subDir){
  if (file.exists(paste(mainDir,  subDir, sep = "")) == F){
    dir.create(paste(mainDir, subDir, sep = ""))
  }  
}  


#function for generating model input images
prepareMatrices  = function(sampleInfo, dataFiltered, matrixTemplate, pictureWidth, pictureHeight, matrixPath)
{ 
  df = dataFiltered 
  createDirIfNotExists(matrixPath, "Matrices")
  pth = paste0(matrixPath, "Matrices", "/")
  createDirIfNotExists(matrixPath, "Images")
  imPth = paste0(matrixPath, "Images", "/")
  for(i in 1:ncol(df))
  {
    output = prepareMatrixFromMap(df, matrixTemplate, pictureWidth,
                                  pictureHeight, i)
    write.table(output, file = paste0(pth, colnames(df)[i], ".tsv"), quote = F, sep = "\t",
                row.names = F, col.names = F)
    png(paste0(imPth, colnames(df)[i], ".png"), width = pictureWidth, height = pictureHeight)
    par(mar = c(0,0,0,0))
    require(grDevices) # for colours
    #image(t(output), xlab = NULL, ylab = NULL)
    image(t(output), col=colorRampPalette(c( "black",  "red",  "red"))(25),  axes=FALSE, mar = c(0,0,0,0),ann  = F)
    dev.off()
  } 
}

# setting the directory for the input images
matrixPath = "./matricesMulticlassFinal/" 
if (file.exists(matrixPath) == F){
  dir.create(matrixPath)
}  

# Generation of input images
prepareMatrices(sampleInfo, countsAll, matrixTemplate,pictureWidth, pictureHeight, matrixPath) 
 
 